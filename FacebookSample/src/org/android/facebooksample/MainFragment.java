package org.android.facebooksample;


import java.util.Arrays;

import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphObject;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.LoginButton;
import com.facebook.*;

import android.content.Intent;
import android.os.Bundle;
import android.renderscript.Sampler.Value;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainFragment extends Fragment{
	private Button batchRequestButton;
	private TextView textViewResults;
	private static final String TAG = "MainFragment";
	private UiLifecycleHelper uiHelper;
	private Session.StatusCallback callback = new SessionStatusCallback();
	private String id;
    static int count =0;
	private class SessionStatusCallback implements Session.StatusCallback {
	    @Override
	    public void call(Session session, SessionState state, Exception exception) {
	            // Respond to session state changes, ex: updating the view
	    }
	}
	private void onClickLogin() {
	    Session session = Session.getActiveSession();
	    if (!session.isOpened() && !session.isClosed()) {
	        session.openForRead(new Session.OpenRequest(this)
	            .setPermissions(Arrays.asList("public_profile"))
	            .setCallback(callback));
	    } else {
	        Session.openActiveSession(getActivity(), this, true, callback);
	    }
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savInstanceState) {
		View view = inflater.inflate(R.layout.activity_main, container, false);
		
		LoginButton authButton = (LoginButton) view.findViewById(R.id.login_button_facebook);
		authButton.setFragment(this);
		authButton.setReadPermissions(Arrays.asList("public_profile"));

		
		
		batchRequestButton = (Button) view.findViewById(R.id.batchRequestButton);
		batchRequestButton.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View v) {
		        doBatchRequest();        
		    }
		});
		return view;
	}

	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	    uiHelper = new UiLifecycleHelper(getActivity(), callback);
	    uiHelper.onCreate(savedInstanceState);
	}


	private void onSessionStateChange(Session session, SessionState state, Exception exception) {
		if (state.isOpened()) {
		       batchRequestButton.setVisibility(View.VISIBLE);
		   } else if (state.isClosed()) {
		       batchRequestButton.setVisibility(View.INVISIBLE);
		   }
	}
	@Override
	public void onResume() {
	    super.onResume();
	    // For scenarios where the main activity is launched and user
	    // session is not null, the session state change notification
	    // may not be triggered. Trigger it if it's open/closed.
	    Session session = Session.getActiveSession();
	    if (session != null &&
	           (session.isOpened() || session.isClosed()) ) {
	        onSessionStateChange(session, session.getState(), null);
	    }
	    uiHelper.onResume();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
	    uiHelper.onActivityResult(requestCode, resultCode, data);
	}
	private void doBatchRequest() {
	    textViewResults = (TextView) this.getView().findViewById(R.id.textViewResults);
	    textViewResults.setText("");

	    String[] requestIds = {"me", "4"};

	    RequestBatch requestBatch = new RequestBatch();
	    for (final String requestId : requestIds) {
	        requestBatch.add(new Request(Session.getActiveSession(), 
	                requestId, null, null, new Request.Callback() {
	            public void onCompleted(Response response) {
	                GraphObject graphObject = response.getGraphObject();
	                String s = textViewResults.getText().toString();
	                if (graphObject != null) {
	                    if (graphObject.getProperty("id") != null && String.valueOf(graphObject.getProperty("id")).length() != 1 && count==0){
	                    	count++;
	                        s = s + String.format(String.valueOf(graphObject.getProperty("id")));
	               
	                        Toast.makeText(getActivity(), String.valueOf(graphObject.getProperty("id")), 3000).show();
	                        
	                    }
	                }
	                textViewResults.setText(s);
	            }
	        }));
	    requestBatch.executeAsync();
	}
}

	@Override
	public void onPause() {
	    super.onPause();
	    uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
	    super.onDestroy();
	    uiHelper.onDestroy();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
	    super.onSaveInstanceState(outState);
	    uiHelper.onSaveInstanceState(outState);
	}
	
}
